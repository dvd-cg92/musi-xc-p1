class Node: 
    
    def __init__(self, name, url):
        self.name = name
        self.url = url
        self.sons = []

    def addSon(self, newSon):
        self.sons.append(newSon)

    def __repr__(self):
        return '=========== \nNode ' + self.name + ' \n\nSons: \n' + "\n".join(self.sons) + ' \n =========== \n'

    def __str__(self):
        return str(self.name)