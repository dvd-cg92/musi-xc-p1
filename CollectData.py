import requests
import time
from Node import Node
from bs4 import BeautifulSoup
from CsvExport import CsvExport

urls = ["https://github.com/tensorflow/tensorflow/network/dependencies",
        "https://github.com/pandas-dev/pandas/network/dependencies",
        "https://github.com/pytorch/pytorch/network/dependencies",
        "https://github.com/scikit-learn/scikit-learn/network/dependencies",
        "https://github.com/iperov/DeepFaceLab/network/dependencies",
        "https://github.com/plotly/plotly.py/network/dependencies",
        "https://github.com/Unity-Technologies/ml-agents/network/dependencies",
        "https://github.com/ageron/handson-ml/network/dependencies",
        "https://github.com/horovod/horovod/network/dependencies",
        "https://github.com/eriklindernoren/ML-From-Scratch/network/dependencies",
        "https://github.com/josephmisiti/awesome-machine-learning/network/dependencies",
        "https://github.com/yhat/rodeo/network/dependencies",
        "https://github.com/jeroenjanssens/data-science-at-the-command-line/network/dependencies",
        "https://github.com/scrapy/scrapy/network/dependencies",
        "https://github.com/dmlc/xgboost/network/dependencies",
        "https://github.com/pydot/pydot/network/dependencies"]

visitedUrls = []

# defined on https://docs.github.com/es/free-pro-team@latest/github/visualizing-repository-data-with-graphs/about-the-dependency-graph
# That are the folders where github will find dependencies
pythonSupportedFiles = ["requirements.txt", "pipfile.lock", "setup.py"]

def getSons(node):
    if any(node.url in s for s in visitedUrls): return
    visitedUrls.append(node.url)
    print(len(visitedUrls))
    if (len(visitedUrls) > 1500): return

    print("GET " + node.url + "\n\n")
    r = requests.get(node.url)
    soup = BeautifulSoup(r.content, "html.parser")

    for dependencyBox in soup.findAll("div", {"class":"Box"}):
        boxTitle = dependencyBox.find('a', {"data-octo-click":"dep_graph_manifest"})

        # Check if box contains python dependencies based on its name
        if (boxTitle and any(format in boxTitle.text for format in pythonSupportedFiles)):
            for dependencyRow in dependencyBox.findAll("div", {"class":"Box-row"}):
                row = dependencyRow.find('a', {"data-hovercard-type":"repository"})

                if (row):
                    url = "https://github.com" + row['href']
                    sonName = row['href'][1:]
                    newSon = Node(sonName, url + "/network/dependencies")
                    # Check if already added
                    if(all(newSon.url != son.url for son in node.sons)):
                        node.addSon(newSon)
                        getSons(newSon)

exportManager = CsvExport()

for url in urls:
    name = url.replace("https://github.com/", "").replace("/network/dependencies", "")
    node = Node(name, url)
    getSons(node)
        
    exportManager.NodeTreeToCsv(node)
