import csv

class CsvExport:

    def NodeTreeToCsv(self, node):
        with open('dependency-relations.csv', 'a+', newline='') as file:
            writer = csv.writer(file)
            self.NodeToCsv(node, writer)
    
    def NodeToCsv(self, node, writer):
        for son in node.sons:
            writer.writerow([node.name, son.name])
            if (son.sons):
                self.NodeToCsv(son, writer)
