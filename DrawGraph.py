import networkx as nx 
import matplotlib.pyplot as plt 

fig = plt.figure(figsize=(40, 40)) 
G = nx.read_edgelist("dependency-relations.csv", delimiter = ",") 
nx.draw_kamada_kawai(G, node_size=30, with_labels=True) 
print(nx.info(G))
plt.axis('equal') 
plt.show() 
fig.savefig('graph.png')